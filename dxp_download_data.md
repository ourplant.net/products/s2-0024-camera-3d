Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0024-camera-3d).

|document|download options|
|:-----|-----:|
|operating manual           |[en](https://gitlab.com/ourplant.net/products/s2-0024-camera-3d/-/raw/main/01_operating_manual/S2-0024_A4_Operating%20Manual.pdf) / [de](https://gitlab.com/ourplant.net/products/s2-0024-camera-3d/-/raw/main/01_operating_manual/S2-0024_A4_Betriebsanleitung.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s2-0024-camera-3d/-/raw/main/09_assembly_drawing/s2-0024_A_ZNB_Camera.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s2-0024-camera-3d/-/raw/main/02_Schaltplan/S2-0024_Camera%20(3D,%204µ%20Auflösung)-B.pdf)|
|maintenance instructions   |[en](https://gitlab.com/ourplant.net/products/s2-0024-camera-3d/-/raw/main/05_maintenance_instructions/S2-0024_A_Maintenance_instructions.pdf) / [de](https://gitlab.com/ourplant.net/products/s2-0024-camera-3d/-/raw/main/05_Wartungsanweisungen/S2-0024_A_Wartungsanweisungen.pdf)|
|spare parts                |[en](https://gitlab.com/ourplant.net/products/s2-0024-camera-3d/-/raw/main/04_spare_parts/S2-0024_A3_EVL_engl.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
