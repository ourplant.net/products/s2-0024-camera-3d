The S2-0024 Camera 3D has the following technical specifications.

|Parameter|Value|
|:----|----:|
|Abmessungen in mm (B x T x H)|49 x 261 x 341|
|Gewicht in kg|4|
|Verfahrbereich in Z in mm	|150|
|Objektivart|Makro-Objektiv|
|Bildfeld in mm (B x H)|7,2 x 5,38|
|Auflösung Kamera in µm	|4|
|Spannung in V|48|
|Max. Stromstärke in A|5|
|Kommunikationsschnittstelle|Ethernet|


<!-- 2021 (C) Häcker Automation GmbH -->