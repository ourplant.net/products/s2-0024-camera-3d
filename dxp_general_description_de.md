Die Camera 3D ist ein Inspektionskopf mit einer optischen 3D-Erkennung und automatischer Positionskorrektur. Mit Hilfe eines stereoskopischen Kamerasystems betrachtet er die Struktur von Bauteil- und Substratoberflächen aus zwei verschiedenen Blickwinkeln. Aus den so erfassten Bilddaten wird eine exakte 3D-Lage berechnet.

Mit der Anlagensteuerungssoftware werden die erfassten Bilddaten verarbeitet. Fehlerkompensations-Algorithmen sorgen dafür, dass Abweichungen von Sollpositionen durch Neuausrichtung der Bearbeitungsköpfe korrigiert werden. Der Z-Achs-Hub von 150 mm ermöglicht zudem die Erkennung von Positionen in sehr unterschiedlichen Höhen.

<!-- 2021 (C) Häcker Automation GmbH -->